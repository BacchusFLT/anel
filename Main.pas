unit Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls, Vcl.FileCtrl, Vcl.ComCtrls;

type
  TForm1 = class(TForm)
    StatusBar1: TStatusBar;
    Panel1: TPanel;
    LabeledEdit1: TLabeledEdit;
    LabeledEdit2: TLabeledEdit;
    LabeledEdit3: TLabeledEdit;
    Panel2: TPanel;
    FontImage: TImage;
    Panel3: TPanel;
    DriveComboBox1: TDriveComboBox;
    DirectoryListBox1: TDirectoryListBox;
    FileListBox1: TFileListBox;
    CharImage: TImage;
    procedure FormCreate(Sender: TObject);
    procedure CharImageMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure CharImageMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure CharImageMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure FileListBox1Change(Sender: TObject);
    procedure FontImageMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;
  Bitmap: TBitmap;
  drawingNow: Boolean;
  S1: File of Byte;

implementation

{$R *.dfm}

procedure SetFontPixel(xpos, ypos: Integer);
var
  xplot, yplot: Integer;
  i, j: Integer;

begin
  for j := 0 to 3 do
  begin
    for i := 0 to 3 do
    begin
      xplot := xpos * 4 + i;
      yplot := ypos * 4 + j;
      Form1.FontImage.Canvas.Pixels[xplot, yplot] := clBlack;
    end;
  end;
end;

procedure ClearFontPixel(xpos, ypos: Integer);
var
  xplot, yplot: Integer;
  i, j: Integer;
begin
  for j := 0 to 3 do
  begin
    for i := 0 to 3 do
    begin
      xplot := xpos * 4 + i;
      yplot := ypos * 4 + j;
      Form1.FontImage.Canvas.Pixels[xplot, yplot] := clWhite;
    end;
  end;
end;

Procedure PlotTheFont;
var
  xcnt, ycnt1, ycnt2: Integer;
  xpos, ypos: Integer;
  tempbyte: Byte;

begin

  AssignFile(S1, Form1.FileListBox1.FileName);
  Reset(S1);

  Read(S1, tempbyte); // Skip start bytes
  Read(S1, tempbyte); // skip startbytes

  ycnt1 := 0;
  repeat

    xcnt := 0;
    repeat

      ycnt2 := 0;
      repeat
        xpos := xcnt * 8; // 8 plus frame
        ypos := ycnt1 * 8 + ycnt2; // 8 plus frame

        Read(S1, tempbyte);

        if ((tempbyte AND 128) = 128) then
          SetFontPixel(xpos, ypos)
        else
          ClearFontPixel(xpos, ypos);

        if ((tempbyte AND 64) = 64) then
          SetFontPixel(xpos + 1, ypos)
        else
          ClearFontPixel(xpos + 1, ypos);

        if ((tempbyte AND 32) = 32) then
          SetFontPixel(xpos + 2, ypos)
        else
          ClearFontPixel(xpos + 2, ypos);

        if ((tempbyte AND 16) = 16) then
          SetFontPixel(xpos + 3, ypos)
        else
          ClearFontPixel(xpos + 3, ypos);

        if ((tempbyte AND 8) = 8) then
          SetFontPixel(xpos + 4, ypos)
        else
          ClearFontPixel(xpos + 4, ypos);

        if ((tempbyte AND 4) = 4) then
          SetFontPixel(xpos + 5, ypos)
        else
          ClearFontPixel(xpos + 5, ypos);

        if ((tempbyte AND 2) = 2) then
          SetFontPixel(xpos + 6, ypos)
        else
          ClearFontPixel(xpos + 6, ypos);

        if ((tempbyte AND 1) = 1) then
          SetFontPixel(xpos + 7, ypos)
        else
          ClearFontPixel(xpos + 7, ypos);

        ycnt2 := ycnt2 + 1;

      until ((ycnt2 = 8) or EOF(S1));

      xcnt := xcnt + 1;
    until ((xcnt = 32) or EOF(S1));

    ycnt1 := ycnt1 + 1;
  until ((ycnt1 = 8) or EOF(S1));

  CloseFile(S1);
end;

procedure SetCharPixel(X, Y: Integer);
var
  xpos, ypos: Integer;
begin
  xpos := X div 8;
  ypos := Y div 8;

  Form1.CharImage.Canvas.Pixels[xpos, ypos] := clBlack;
end;

procedure TForm1.FileListBox1Change(Sender: TObject);
begin
  if FileListBox1.FileName <> '' then
  begin
    StatusBar1.Panels[0].Text := 'Selected:' + FileListBox1.FileName;

    PlotTheFont;

  end;

end;

procedure TForm1.FontImageMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
var
  CharX, CharY,TheChar : Integer;
  
begin

  CharX := X DIV (8*4);  //Char width times screen pixels per c64 pixel
  CharY := Y DIV (8*4);  //Char height times screen pixels per c64 pixel

  TheChar := CharY*32+CharX;
 
  StatusBar1.Panels[1].Text := 'X: ' + IntToStr(CharX) + '  Y: ' + IntToStr(CharY) + '  Char: $' + IntToHex(TheChar,2) ;

  LabeledEdit1.Text:=  'Char: $' + IntToHex(TheChar,2);

  // 
  
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  drawingNow := False;
  CharImage.Picture.Bitmap.SetSize(8, 8);
  CharImage.Picture.Create;
  CharImage.Canvas.Pen.Color := clBlack;
end;

procedure TForm1.CharImageMouseDown(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);

begin
  drawingNow := True;
  SetCharPixel(X, Y);

end;

procedure TForm1.CharImageMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
begin
  if drawingNow then
    SetCharPixel(X, Y);
end;

procedure TForm1.CharImageMouseUp(Sender: TObject; Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  drawingNow := False;
end;

end.
