object Form1: TForm1
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 
    'Anel 0.1  - Filling the Font Void - (c) 2018 Pontus "Bachcus" Be' +
    'rg'
  ClientHeight = 671
  ClientWidth = 1068
  Color = cl3DLight
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TStatusBar
    Left = 0
    Top = 652
    Width = 1068
    Height = 19
    Panels = <
      item
        Width = 400
      end
      item
        Width = 400
      end
      item
        Text = 'Anel 0.1 (c) Pontus "Bacchus" Berg'
        Width = 50
      end>
  end
  object Panel1: TPanel
    Left = 776
    Top = 8
    Width = 274
    Height = 349
    TabOrder = 1
    object CharImage: TImage
      Left = 18
      Top = 160
      Width = 64
      Height = 64
      Cursor = crCross
      ParentShowHint = False
      Proportional = True
      ShowHint = True
      Stretch = True
      Visible = False
      OnMouseDown = CharImageMouseDown
      OnMouseMove = CharImageMouseMove
      OnMouseUp = CharImageMouseUp
    end
    object LabeledEdit1: TLabeledEdit
      Left = 16
      Top = 25
      Width = 121
      Height = 21
      EditLabel.Width = 62
      EditLabel.Height = 13
      EditLabel.Caption = 'Font position'
      ReadOnly = True
      TabOrder = 0
    end
    object LabeledEdit2: TLabeledEdit
      Left = 16
      Top = 73
      Width = 121
      Height = 21
      EditLabel.Width = 63
      EditLabel.Height = 13
      EditLabel.Caption = 'PETSCII char'
      ReadOnly = True
      TabOrder = 1
      Text = 'Not implemented yet'
    end
    object LabeledEdit3: TLabeledEdit
      Left = 16
      Top = 121
      Width = 121
      Height = 21
      EditLabel.Width = 80
      EditLabel.Height = 13
      EditLabel.Caption = 'Screencode char'
      ReadOnly = True
      TabOrder = 2
      Text = 'Not implemented yet'
    end
  end
  object Panel2: TPanel
    Left = 8
    Top = 363
    Width = 1041
    Height = 273
    TabOrder = 2
    object FontImage: TImage
      Left = 9
      Top = 9
      Width = 1024
      Height = 256
      IncrementalDisplay = True
      OnMouseMove = FontImageMouseMove
    end
  end
  object Panel3: TPanel
    Left = 8
    Top = 8
    Width = 762
    Height = 349
    TabOrder = 3
    object DriveComboBox1: TDriveComboBox
      Left = 8
      Top = 7
      Width = 337
      Height = 19
      DirList = DirectoryListBox1
      TabOrder = 0
    end
    object DirectoryListBox1: TDirectoryListBox
      Left = 8
      Top = 32
      Width = 337
      Height = 304
      FileList = FileListBox1
      TabOrder = 1
    end
    object FileListBox1: TFileListBox
      Left = 351
      Top = 7
      Width = 402
      Height = 329
      ItemHeight = 13
      TabOrder = 2
      OnChange = FileListBox1Change
    end
  end
end
