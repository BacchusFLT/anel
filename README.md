(Font)Anel - Filing the Font Voice

(c) 2018, By Pontus "Bacchus" Berg, 

Purpose:
Anel is a program to view c64 font files. Input assumes a start address for the file (which is disregarded but needs to be there).

Status:
Highly premature. It opens a 1x1 font nicely and displays the content, but that's sort of what it does.

Error checking of the input data is minimal to say the least. Opening the exe file of the program as a font and you get an error, for starters.

To do:
* Better input error checking
* Reference of the looks of the equivalent ROM font
* Reference between PETSCII and SCREENCODE representation. (What char in the respective form would usage as either require).
* Font editing (at least the basics for quick fixing like adding a missing char or so)

There is alreay a bit of the core functionaity for editing (putting pixels on an 8x8 pixel grid), but it's disabled in the exe verison.